---
---
# WebUntis

WebUntis ist eine Software des österreichischen Herstellers *Gruber und Petters*, die Funktionen wie Online Stundenpläne, Ressourcenplanung und Klassenbücher anbietet.

## Wie kann ich mich da anmelden?
Jeder Schüler hat einen Zugang. Dieser sieht wie folgt aus:

- Nutzername: Nnn,Vnn,JJJJMMTT (Nnn=erste drei Buchstaben des Nachnamen, Vnn=erste drei Buchstaben des Vornamen, JJJJMMTT=Geburtsdatum)
  - z. B. Bal,Ben,19990805
- Passwort: Geburtsdatum im Format JJMMDD
  - z. B. 19990805 für den 5. August 1999

Die Internetadresse von WebUntis ist <a href="https://webuntis.cjd-rostock.de">webuntis.cjd-rostock.de</a>. Am besten setzt man sich ein Lesezeichen oder etwas ähnliches. Die URL findet man auch auf der Schulwebsite unter <a href="http://www.cjd-rostock.de/info-portal/">Info-Portal</a> auf der rechten Seite.

## Was kann ich da machen?
Die wichtigste Funktion, die Schüler aktuell auf WebUntis nutzen können, ist der Stundenplan. Im Gegensatz zu dem Stundenplan, den man auf Chrisys findet, gibt es auf WebUntis die Möglichkeit sich seinen eigenen personalisierten Stundenplan anzeigen zu lassen. Das ist besonders für Schüler im Kurssystem sehr nützlich. Außerdem kann man sich den Stundenplan auch in der App *UntisMobile* anzeigen lassen.

Außerdem können Lehrer Schülern Nachrichten auf WebUntis schreiben. Umgekehrt können Schüler ihren Klassenleitern Nachrichten senden.

Eine Funktion, deren Potential aktuell noch nicht voll ausgenutzt wird, ist der Test- und Klausurplan. Lehrer müssen in WebUntis ihre Tests und Klausuren eintragen, wie in einem Klassenbuch. Schüler sollen darauf Zugriff haben. Das ist aktuell nicht immer der Fall. Wir setzen uns aber dafür ein, dass das verbessert wird.
