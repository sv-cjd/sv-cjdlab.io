---
title: "\"Wann bekommen wir wieder einen Schülerclub?\""
summary: Oft werden wir gefragt, wann es wieder einen Schülerclub geben wird. Hier die Antwort.
author: Ben Bals, 11b
---

Wie viele der Schüler, die schon etwas länger an unserer Schule sind wissen, gab es vor bis vor einiger Zeit
einen sog. "Schülerclub".
In diesem konnte man Essen, so wie belegte Brötchen oder Terinen, aber auch etwas Süßes erwerben.

Seid die Internatsküche aufgrund der Baumaßnahmen umziehen musste, wurde diese in die Räumlichkeiten des
Schülerclubs verlegt und dieser vorrübergehend geschlossen.

Darin liegt auch die Antwort auf die obengenannte Fragen.
Sobald die Baumaßnahmen am Internat abgeschlossen sind
und die Internatsküche wieder zurück kann,
werden die Räumlichkeiten wieder frei und der Schülerclub kann seinen Betrieb
wieder aufnehmen.
