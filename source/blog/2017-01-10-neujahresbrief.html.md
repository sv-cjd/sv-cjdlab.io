---
title: Neujahresbrief
summary: Anlässlich des neuen Jahres wendet sich die Schülervertretung an alle Schülerinnen und Schüler sowie Eltern des CJD Rostock.
author: Ben Bals, 11b
---
Anlässlich des neuen Jahres haben wir uns am 10. Januar 2017 an alle Schülerinnen und Schüler sowie Eltern mit [diesem Brief](/infos/dokumente/rundbriefe/Neujahresbrief.pdf) gewendet.
