---
title: Beschmierung der Toiletten am 12. Januar
summary: Stellungnahme der Schülevertretung zur Verunstaltung der Jungstoiletten in Haus 2
author: Ben Bals, 11b
---

Liebe Schülerinnen und Schüler,

wir sind schockiert und entsetzt von den Beschmierungen der Toiletten. Am Nachmittag des 12. Januars wurde festgestellt, dass die Jungstoiletten in Haus 2 mit Schriftzügen, wie „ACAB” und „FCH” verunstaltet wurden.

Ein solches Verhalten ist für uns unbegreiflich. Die mutwillige Beschädigung fremden Eigentums wiegt besonders schwer, da die betroffenen Flächen Teil des öffentlichen Raums unserer Schule sind. Sie zu verunstalten bedeutet also, den Schulraum für alle Schüler zu verschlechtern.

Wir sind eine Gemeinschaft und das ist schlicht und einfach ein Verrat an dieser und an allen Schülern. Daher appellieren wir an alle von Euch: Tut so etwas nicht und wenn Ihr es bemerkt, dann schweigt nicht, sondern erhebt Eure Stimme. Nun mögen manche denken: „Aber ich bin doch keine Petze!” Doch das hat nichts mehr mit petzen zu tun. Wenn jemand so das Vertrauen hintergeht, das ihm hier zuteil wird, dann muss er die Konsequenzen akzeptieren.

Melden könnt Ihr Euch persönlich bei uns, in unserem Briefkasten und unter [hallo.sv-rostock@cjd-nord.de](mailto:hallo.sv-rostock@cjd-nord.de). Wir werden eure Anonymität schützen.

Wir möchten an dieser Stelle die Gelegenheit nutzen, um all denen zu danken, die mit unserer Schule sorgsam umgehen und sie zu dem Ort des Miteinanders machen, die sie ist. Danke.

Wenn Ihr das auch so seht wie wir, bitten wir Euch die Unterschriftenliste, die Eure Lehrer mit dem Papierausdruck dieses Brief erhalten, zu unterschreiben. Damit könnt Ihr zeigen, dass solch ein Verhalten in unserer Gemeinschaft nicht toleriert wird. Außerdem habt ihr die Chance, einen kurzen Kommentar zu hinterlassen. Falls Euch der Platz für einen Kommentar zu klein ist, könnt ihr ihn uns auch gerne mailen oder ihn in unseren Briefkasten stecken.

Hoffnungsvoll

Die Schülervertretung am CJD Rostock
