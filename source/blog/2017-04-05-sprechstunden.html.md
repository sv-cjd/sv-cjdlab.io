---
title: Sprechstunde der Schülervertretung
summary: Die Schülervertretung stellt sich von nun an alle zwei Wochen in der Frühstückspause Euren Fragen.
author: Ben J. Bals, Schülersprecher
---

Liebe Schülerinnen und Schüler,

die Schülervertretung hat heute eine Satzungsänderung beschlossen. Ein wichtiger Teil davon ist, dass wir von nun an alle zwei Wochen in der Frühstückspause eine Sprechstunde durchführen werden. Dort werden drei von uns Eure Fragen beantworten und auf Eure Anmerkungen eingehen.

Die Termine werden wir vorher an unseren Pinnwänden aushängen und via unserem E-Mail-Verteiler bekanntgeben.

Wir hoffen diese Maßnahme bringt die Schülervertretung allen Schülern näher.

Mit besten Wünschen

Die Schülervertretung am CJD Rostock
