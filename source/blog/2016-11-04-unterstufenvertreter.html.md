---
title: Wahl der Unterstufenvertreter
summary: Wir haben Vertreter für die Stufen 5 bis 7 wählen lassen. Warum und mehr hier.
author: Ben
---

Liebe Schülerinnen und Schüler,

am Dienstag, den 1. November, haben wir Vertreter für die Klassenstufen 5 bis 7 von und aus den Klassensprechern dieser Stufen wählen lassen.
Diese sogenannten Stufensprecher werden bis zur Neuwahl Teil der Schülervertretung sein.

Dabei handelt es sich um

- Julian Fiebig, Klasse 7c,
- Hannah, Klasse 6b,
- Willhelm, Klasse 6b,
- Klara Roloff, Klasse 5d.

**Warum?**

Dadurch, dass alle Vertreter seit ihrer Wahl um eine Klasse aufgestiegen sind und eine Vertreterin aus der Schülervertretung ausgeschieden ist, wurden aus den vormals drei Vertretern dieser Stufen null.

Auf dem Klassensprecherseminar in Ibenhorst wurde unser Wunsch nach Vertretern aus diesen Stufen, die für ihre Probleme, Sorgen und Bedürfnisse einstehen können, bestätigt. Die Klassensprecher zeigten nicht nur den Willen, sondern auch die Fähigkeiten, diesen Wünschen erfolgreich nachzukommen.

Eure Schülervertretung

