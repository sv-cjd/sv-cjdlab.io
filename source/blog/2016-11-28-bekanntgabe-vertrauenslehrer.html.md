---
title: Wahl der Vertrauenslehrer
summary: Am 28. 11. haben alle Schüler die neuen Vertrauenslehrer Frau Langer und Herrn Brandt gewählt.
author: Ben
---

Liebe Schülerinnen und Schüler,

am 28. 11. habt ihr in einer von uns verantstalteten Wahl die Vertrauenslehrer gewählt.

Ein Vertrauenslehrer ist eine Person, die für uns Schüler da ist. Bei Problemen oder Sorgen jeglicher Art hat er oder sie immer ein offenes Ohr für uns und versucht gemeinsam mit uns eine Lösung zu finden. Besonders dabei ist, dass wir ihm oder ihr vollständig vertrauen können, denn ein Vertrauenslehrer darf und wird niemandem davon erzählen. Er oder sie versucht immer für euch ansprechbar zu sein und sich unmittelbar Zeit zu nehmen oder möglichst bald einen persönlichen Gesprächstermin zu finden.

Die Sieger der Wahl sind

- Frau Langer
- Herr Brandt.


Wir baten sie, ein Motto für ihr Amt zu formulieren, damit ihr, und vor allem die von euch, die sie nicht persönlich kennen, einen Eindruck von euren neuen Vertrauenslehrern bekommt.

- Frau Langer: *Die Nacht, in der das Fürchten wohnt, hat auch die Sterne und den  Mond. - Mascha Kaléko*
- Herr Brandt: *Kontrolle ist gut, Vertrauen ist besser.*

Wir wünschen den beiden viel Erfolg bei Ihrer Aufgabe.

Genaue Zahlen zur Wahl findet ihr [hier](https://docs.google.com/spreadsheets/d/1--L68gVALzjSqJwFvhIvwJyTo6tqgypTghex3w2FnDo/edit?usp=sharing).

Eure Schülervertretung
