---
title: E-Mail-Verteiler
summary: Die Schülervertretung möchte die Schüler möglichst gut über ihre Aktivitäten informieren. Dafür möchten wir nun E-Mails verschicken.
author: Ben J. Bals, Schülersprecher
---

Liebe Schülerinnen und Schüler,

Offenheit und Kommunikation mit Euch sind uns wichtig. Wir vertreten eure Interessen und wollen das nicht im stillen Kämmerlein tun. Daher bitten wir alle Klassensprecher um ihre E-Mail-Adresse. Jeder, der an unseren Aktivitäten interessiert ist, kann sich ebenfalls eintragen lassen. Wir werden Euch via E-Mail regelmäßig über unsere Arbeit auf dem Laufenden halten.

Einen entsprechenden Brief haben wir am 31. März an alle Klassenleiter gegeben. Wenn Ihr auch in den Verteiler möchtet, schickt uns einfach eine E-Mail an [hallo.sv-rostock@cjd-rostock.de](mailto:hallo.sv-rostock@cjd-nord.de).

Natürlich lesen wir alle Kommentare, die Ihr uns per E-Mails schickt. Also nutzt die Möglichkeit und schreibt uns!

Mit besten Wünschen

Die Schülervertretung am CJD Rostock
